from checkers.piece import*
from math import floor
from checkers.endgame_tab import Endshpile
import sqlite3 as sq

class Board:
    def __init__(self, win, pl):
        self.win = win
        self.board = []
        self.black_left = self.white_left = 12
        self.black_crowns = self.white_crowns = 0
        self.white_crowns_am = self.black_crowns_am = 0
        self.pl = pl
        self.b_score = self.w_score = 0
        self.create_board()
    
    def draw_squares(self, win):
        '''image = pygame.image.load(os.path.abspath('./sourses/pics/field.png'))  #COMMENT FOR OPTIMIZATION
        image = pygame.transform.scale(image, (800, 800))   #COMMENT FOR OPTIMIZATION
        self.win.blit(image, (0, 0))'''
        win.fill(BRAWN)  #UNCOMMENT FOR OPTIMIZATION
        for row in range(ROWS):
            for col in range(row % 2, COLS, 2):
                pygame.draw.rect(win, WHITE, (row*SQUARE_SIZE, col *SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))

    def move(self, piece, row, col):
        self.board[piece.row][piece.col], self.board[row][col] = self.board[row][col], self.board[piece.row][piece.col]
        piece.move(row, col)

        if row == ROWS - 1 or row == 0:
            piece.make_crown()
            if piece.color == WHITE:
                self.white_crowns += 1
            else:
                self.black_crowns += 1 

    def get_piece(self, row, col):
        return self.board[row][col]

    def create_board(self):
        for row in range(ROWS):
            self.board.append([])
            for col in range(COLS):
                if col % 2 == ((row +  1) % 2):
                    if row < 3:
                        self.board[row].append(Piece(row, col, WHITE))
                    elif row > 4:
                        self.board[row].append(Piece(row, col, BLACK))
                    else:
                        self.board[row].append(0)
                else:
                    self.board[row].append(0)
        
    def draw(self, win):
        self.draw_squares(win)
        for row in range(ROWS):
            for col in range(COLS):
                piece = self.board[row][col]
                if piece != 0:
                    piece.draw(win)

    def remove(self, pieces):
        for piece in pieces:
            self.board[piece.row][piece.col] = 0
            if piece != 0:
                if piece.color == BLACK:
                    self.black_left -= 1
                else:
                    self.white_left -= 1
    
    def winner(self):
        if self.black_left <= 0:
            return WHITE
        elif self.white_left <= 0:
            return BLACK
        
        return None 

    def check_moves(self, piece, dir):
        x, y = piece.row, piece.col
        valid_moves = {}
        try:
            if 0 < x+dir < 8 and y < 6:
                if self.board[x+dir][y+1] == 0:
                    valid_moves[(x+dir, y+1)] = []
            if 0 < x + dir < 8 and y > 0:
                if self.board[x+dir][y-1] == 0:
                    valid_moves[(x+dir, y-1)] = []

            if 1 < x+dir < 7 and y < 5:
                if self.board[x+dir][y+1] != 0 and self.board[x+dir*2][y+2] == 0 and self.board[x+dir][y+1].color != piece.color:
                    valid_moves[(x + dir*2, y + 2)] = [self.board[x+dir][y+1]]
            if 1 < x + dir < 7 and y > 1:
                if self.board[x+dir][y-1] != 0 and self.board[x+dir*2][y-2] == 0 and self.board[x+dir][y+1].color != piece.color:
                    valid_moves[(x + dir*2, y - 2)] = [self.board[x+dir][y-1]]
        except:
            pass
        return valid_moves

    def get_valid_moves(self, piece):
        moves = {}

        if piece.color == BLACK:
            moves.update(self.check_moves(piece, -1))
        if piece.color == WHITE:
            moves.update(self.check_moves(piece, 1))
        if piece.crown:
            pass

        return moves


    def endgame(self, t):
        time = str(floor(t)//60) + ':' + str(floor(t)%60)
        if self.white_left != 0:
            res  = 'WIN!'
            score = int(12 + self.white_left*10 + self.white_crowns*10 + 11111//t)
        else:
            res = 'LOSE!'
            tmp = int((self.black_left)*10 + self.white_crowns*10 - 11111//t)
            score = tmp if tmp > 0 else 0

        out = [res, 'Checkers left: ' + str(self.white_left),
               'Crowns: ' + str(self.white_crowns),
               'Spent time: ' + time,
               'Total score: ' + str(score)]
        end = Endshpile([self.pl, out])
        end.finish()