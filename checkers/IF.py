import pygame
import os
  
class InputField:
    def __init__(self, disp, t, l, w, h, p):
        self.display = disp
        self.base_font = pygame.font.Font(os.path.abspath('./sourses/fonts/DancingScript-VariableFont_wght.ttf'), 32)
        self.user_text = ''
        self.input_rect = pygame.Rect(t, l, w, h)
        self.rect = pygame.Rect(t-2, l-2, w+4, h+4)
        self.is_pass = p
        self.realpass = ''
        self.color_active = pygame.Color('#E5E8C9')
        self.color_passive = pygame.Color('#D8D4D1')
        self.color = self.color_passive
        self.active = False
        
    def show(self):
        pygame.draw.rect(self.display, (0, 0, 0), self.rect)
        pygame.draw.rect(self.display, self.color, self.input_rect)
        text_surface = self.base_font.render(self.user_text, True, (0, 0, 0))
        self.display.blit(text_surface, (self.input_rect.x+5, self.input_rect.y+5))

    def Update(self, event):
        self.show()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.input_rect.collidepoint(event.pos):
                self.active = True
            else:
                self.active = False

        if event.type == pygame.KEYDOWN and self.active:
            if event.key == pygame.K_BACKSPACE:
                if not self.is_pass:
                    self.user_text = self.user_text[:-1]
                else:
                    self.user_text = self.user_text[:-1]
                    self.realpass = self.realpass[:-1]
            else:
                if len(self.user_text)<10 and not self.is_pass:
                    self.user_text += event.unicode
                elif len(self.user_text)<10 and self.is_pass:
                    self.user_text += "*"
                    self.realpass += event.unicode
    
        if self.active:
            self.color = self.color_active
        else:
            self.color = self.color_passive
            
        pygame.draw.rect(self.display, (0, 0, 0), self.rect)
        pygame.draw.rect(self.display, self.color, self.input_rect)
        text_surface = self.base_font.render(self.user_text, True, (0, 0, 0))
        self.display.blit(text_surface, (self.input_rect.x+5, self.input_rect.y+5))
        pygame.display.flip()
        
    def clear(self):
        if self.user_text != '' and self.is_pass:
            out = self.realpass
            self.user_text = ''
            self.realpass = ''
            return out
        elif self.user_text != '' and not self.is_pass:
            out = self.user_text
            self.user_text = ''
            return out 
        else:
            print("none input")
            return ''

