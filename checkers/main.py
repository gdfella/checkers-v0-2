import time
from checkers.constants import *
from checkers.game import Game

def get_row_col_from_mouse(pos):
    x, y = pos
    row = y // SQUARE_SIZE
    col = x // SQUARE_SIZE
    return row, col

def Start(pl_name):

    WIN = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption('Ultra-Checkers HD playing by ' + pl_name)
    icon = pygame.image.load(os.path.abspath('./sourses/pics/icon.png'))
    pygame.display.set_icon(icon)

    run = True
    game = Game(WIN, pl_name)
    start = time.time()
    is_ended = True
    while run:

        if game.winner() != None:
            print(game.winner())
            run = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                is_ended = False
            
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                row, col = get_row_col_from_mouse(pos)
                game.select(row, col)

        game.update()
    stop = time.time()
    timer = stop - start

    if is_ended:
        game.board.endgame(timer)
        pygame.quit()
        quit()
        return game.board.w_score