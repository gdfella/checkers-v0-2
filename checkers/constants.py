import pygame
import os.path

WIDTH, HEIGHT = 800, 800
ROWS, COLS = 8, 8
SQUARE_SIZE = WIDTH//COLS

WHITE = (234, 182, 126, 92)
BLACK = (79, 52, 16, 31)
BRAWN = (102, 62, 20)
GREY = (0,0,0)
GREEN = (0, 255, 0, 30)
SELECTED = (255, 0, 0)

PICS = {BLACK: 'sourses/pics/black.png', WHITE: 'sourses/pics/white.png', GREEN: 'sourses/pics/tip.png', SELECTED: 'sourses/pics/selected.png'}
CROWN = pygame.transform.scale(pygame.image.load(os.path.abspath('./sourses/pics/crown.png')), (44, 25))
