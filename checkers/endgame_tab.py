import pygame
from os import path
class Endshpile:
    def __init__(self, info):
        self.info = info
    def finish(self):
        pygame.init()
        bg = pygame.image.load(path.abspath('./sourses/pics/end_bg.png'))
        bg = pygame.transform.scale(bg, (450, 300))
        screen = pygame.display.set_mode((450, 300))
        pygame.display.set_caption('Well played, {}!'.format(self.info[0]))


        running = True
        font = pygame.font.Font(path.abspath('./sourses/fonts/DancingScript-VariableFont_wght.ttf'), 26)
        #label =
        labels = []
        for i in self.info[1]:
            labels.append(font.render(i, True, (255, 255, 255)))

        while running:
            screen.blit(bg, (0, 0))
            for i in range(len(labels)):
                screen.blit(labels[i], (130, 95+i*35))
            pygame.display.flip()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
