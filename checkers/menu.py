import pygame
import os.path
from checkers.IF import InputField
import sqlite3 as sq
from checkers.main import Start

class Button:

    hovered = False
    
    def __init__(self, win, img, pos):
        self.win = win
        self.img = img
        self.pos = pos
        self.image = pygame.image.load(os.path.abspath('./sourses/pics/' + self.img))
        self.image = pygame.transform.scale(self.image, (170, 35))
        self.rect = self.image.get_rect()
        self.rect.topleft = pos
        self.draw()
            
    def draw(self):        
        self.win.blit(self.image, self.pos)
        
    def get_color(self):
        if self.hovered:
            return (244, 230, 213)
        else:
            return (150, 150, 150)

class Menu:
    def __init__(self, win, bg):
        self.is_login = False
        self.log = ''
        self.passw = ''
        self.win = win
        self.bg = bg

    def get_info(self):
        print('DB content:\n')
        with sq.connect(os.path.abspath('../sashki.db')) as con:
            cur = con.cursor()
            cur.execute("SELECT * FROM users")
            rows = cur.fetchall()
        for row in rows:
            print(row)
        return rows

    def upd_score(self, sc, pl):
        with sq.connect(os.path.abspath('../sashki.db')) as con:
            cur = con.cursor()
            cur.execute("Update users SET timeofbest = ? where login = ?;", (sc, pl))

    def push(self, l, p):
        with sq.connect(os.path.abspath('../sashki.db')) as con:
            cur = con.cursor()
            cur.execute("INSERT or IGNORE INTO users VALUES (?,?,?)",(l, p, ""))

    def check_user(self, cont, usr):
        for i in cont:
            if i[0] == usr:
                return True

    def remove_row(self, row):
        with sq.connect(os.path.abspath('../sashki.db')) as con:
            cur = con.cursor()
            cur.execute('DELETE FROM users WHERE login=?', (row,))

    def main_screen(self):
        options = [Button(self.win, "sign_button.png", (150, 105)), Button(self.win, "opt_button.png", (150, 155))]
        for option in options:
            option.draw()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.MOUSEBUTTONDOWN and options != []:
                if options[0].rect.collidepoint(pygame.mouse.get_pos()): 
                    self.is_login = True

    def log_screen(self, login_input, pass_input):
        info = pygame.image.load(os.path.abspath('./sourses/pics/info.png'))
        self.win.blit(info, (280, 90))
        login_input.show()
        pass_input.show()
        conf_butt = Button(self.win, "conf_button.png", (10, 210))
        conf_butt.draw()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            login_input.Update(event)
            pass_input.Update(event)
            if event.type == pygame.MOUSEBUTTONDOWN:
                if conf_butt.rect.collidepoint(pygame.mouse.get_pos()):
                    info = []
                    info.append(login_input.clear())
                    info.append(pass_input.clear())
                    print(info)
                    check = True
                    for i in info:
                        if i == '':
                            check = False
                            break
                    if check:
                        if self.check_user(self.get_info(), info[0]):
                            new_score = Start(info[0])
                            self.upd_score(new_score, info[0])
                        self.push(info[0], info[1])


    
    def clear(self):
        self.win.blit(self.bg, (0,0))

    def run(self, l_in, p_in):
        
        playlist = [os.path.abspath('./sourses/stuff/stuff{}.mp3'.format(i)) for i in range(4)]
        count = 0
        pygame.mixer.music.load(playlist[count])
        count+=1
        pygame.mixer.music.queue(playlist[count])
        pygame.mixer.music.set_endevent(pygame.USEREVENT)
        pygame.mixer.music.play()       
        
        while True:
            display.blit(self.bg, (0,0))
            if not self.is_login:
                self.main_screen()
            else:
                self.log_screen(l_in, p_in)

            for event in pygame.event.get():
                if event.type == pygame.USEREVENT:
                        count = (count+1)%len(playlist)
                        pygame.mixer.music.queue(playlist[count])
            pygame.display.update()
        



pygame.init()
display = pygame.display.set_mode((480, 320))
icon = pygame.image.load(os.path.abspath('./sourses/pics/icon.png'))
bg = pygame.image.load(os.path.abspath('./sourses/pics/bg.png'))
bg = pygame.transform.scale(bg, (480, 320))
pygame.display.set_icon(icon)
pygame.display.set_caption('Ultra-Checkers HD')
login_input = InputField(display, 10, 90, 170, 40, False)
pass_input = InputField(display, 10, 150, 170, 40, True)

menu = Menu(display, bg)
menu.get_info()
menu.run(login_input, pass_input)
